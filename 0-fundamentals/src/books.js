import img1 from "./images/book1.jpg";
import img2 from "./images/book2.jpg";
import img3 from "./images/book3.jpg";

export const books = [
  {
    id: 1,
    author: "Kristin Hannah",
    title: "The Women: A Novel Hardcover",
    img: img1,
  },
  {
    id: 2,

    author: "Adam Wallae",
    title: "How to Catch a Leprechaun",
    img: img2,
  },
  {
    id: 3,
    author: "Ms. Rachell",
    title: "Special Surprise",
    img: img3,
  },
];
