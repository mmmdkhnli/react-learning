import React from "react";
import ReactDOM from "react-dom/client";

// ---------------------------------------------------------
// --------------------FIRST COMPONENT----------------------
// ---------------------------------------------------------

// function Greeting() {
//     return (
//         <>
//             <div className='container'>
//                 <h3>Hello Guys</h3>
//                 <ul>
//                     <li>Ahmad Mammadkhanli</li>
//                     <li>Aydan Talib</li>
//                 </ul>
//             </div>
//         </>
//     )
// }

// function Greeting() {
//     return React.createElement('h5', {}, 'Hello World');
// }

// const root = ReactDOM.createRoot(document.getElementById("root"));

// root.render(<Greeting />);

// ---------------------------------------------------------
// -------------------NESTED COMPONENTS---------------------
// ---------------------------------------------------------

// function Greeting() {
//   return (
//     <div>
//       <Person />
//       <Message />
//     </div>
//   );
// }

// const Person = () => <h2>Ahmad Mammadkhanli</h2>;
// const Message = () => <p>This is my message</p>;

// const root = ReactDOM.createRoot(document.getElementById("root"));

// root.render(<Greeting />);

// ---------------------------------------------------------
// -------------------AMAZON BESTSELLER---------------------
// ---------------------------------------------------------
import "./index.css";

import { books } from "./books";
import Book from "./Book";

function BookList() {
  return (
    <>
      <h1>amazon best sellers</h1>
      <section className="book-list">
        {/* <Book {...firstBook}>
        <p>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eum corrupti
          voluptatem, sint voluptas vitae quibusdam pariatur non aliquam magnam
          omnis, tempora quis nihil illo quia doloribus fuga nam earum
          veritatis!
        </p>
        <button>Click me</button>
      </Book> */}
        {books.map((book, index) => {
          return <Book {...book} key={book.id} index={index} />;
        })}
      </section>
    </>
  );
}

// const Book = (props) => {
//   const { img, title, author, children } = props;

//   return (
//     <article className="book">
//       <img src={img} alt={title} />
//       <h2>{title}</h2>
//       <h4>{author}</h4>
//       {children}
//     </article>
//   );
// };

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(<BookList />);
