import Starter from "./tutorial/01-useState/starter/01-error-example";
import Final from "./tutorial/01-useState/final/01-error-example";

function App() {
  return (
    <div className="container">
      <h2>Advanced React</h2>
      <Starter />
      <Final />
    </div>
  );
}

export default App;
